<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>お問い合わせフォーム</title>
</head>
<body>
<?php 
	// お問い合わせタイトル、詳細のセット
	$title = htmlspecialchars($_POST['title'],ENT_QUOTES);
	$message = htmlspecialchars($_POST['message'],ENT_QUOTES);
?>

<h2>お問い合わせ内容を確認してください</h2>
<br>
<form action="send_inquiry.php" method="POST">
	<input type="hidden" name="title"
	value="<?php echo $title; ?>">
	<input type="hidden" name="message"
	value="<?php echo $message; ?>">
<h3>お問い合わせタイトル: </h3>
<p><?php echo $title; ?></p>
<br>
<br>
<h3>お問い合わせ内容詳細: </h3>
<p><?php
	// 改行部分にBRタグを埋め込む
	echo nl2br($message);
?></p>
<br>
	<input type="submit" value="お問い合わせ内容の送信">
</form>
</body>
</html>
